﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;



public class AuthService
{
    private AppContext context;
    private readonly AppSettings appSettings;
  

    public AuthService(IOptions<AppSettings> appSettings, AppContext context)
    {
        this.appSettings = appSettings.Value;
        this.context = context;
    }

    public User Authenticate(LoginRequest request)
    {
        User user = null;
        UserDB userDb = context.Users.Find(request.Username);
        if (userDb != null)
        {
            if (BCrypt.Net.BCrypt.Verify(request.Password, userDb.Password))
            {
                user = InstantiateUser(userDb);
                user.Token = CreateToken(user);
            }
        }

        try
        {
            context.SaveChanges();
        }
        catch (DbUpdateException)
        {
            user = null;
        }

        return user;
    }

    public bool ChangePassword(ChangePassRequest request)
    {
        bool result = false;

        UserDB userDb = context.Users.Find(request.Username);
        if (userDb != null)
        {
            if (BCrypt.Net.BCrypt.Verify(request.CurrentPassword, userDb.Password))
            {
                userDb.Password = CreateHash(request.NewPassword);
                result = true;
            }

        }

        try
        {
            context.SaveChanges();
        }
        catch(DbUpdateException)
        {
            result = false;
        }

        return result;
    }

    public User CreateUser(LoginRequest request)
    {
        User user = null;

        string hash = CreateHash(request.Password);

        UserDB userDB = new UserDB(request.Username, hash, Role.USER);


        context.Add<UserDB>(userDB);

        try
        {
            context.SaveChanges();
            user = new User(request.Username, Role.USER);
            user.Token = CreateToken(user);
        }
        catch(DbUpdateException)
        {

        }

        return user;
    }

    private string CreateHash(string password)
    {
        string salt = BCrypt.Net.BCrypt.GenerateSalt();
        return BCrypt.Net.BCrypt.HashPassword(password, salt);
    }

    private string CreateToken(User user)
    {
        JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
        byte[] key = Encoding.ASCII.GetBytes(appSettings.Secret);
        SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role)
            }),
            Expires = DateTime.UtcNow.AddDays(1),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature),

        };
        SecurityToken token = handler.CreateToken(descriptor);
        return handler.WriteToken(token);
    }



    private User InstantiateUser(UserDB userDb)
    {
        return new User(userDb.Username, userDb.Role);
    }
}
