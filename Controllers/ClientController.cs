using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;


using System.Linq;

using Microsoft.EntityFrameworkCore;

/**
 * TODO: Make it async/able to run threads and play nice with each other.
 **/

[Route("api/[controller]")]
[ApiController]
public class ClientController : Controller
{
    //change to be more secure
    private AppContext context;

    public ClientController(AppContext context)
    {
        this.context = context;
    }

    [HttpGet("")]
    public ActionResult<IEnumerable<ClientData>> Clients()
    {
        List<ClientData> ClientList = context.Clients.ToList<ClientData>();

        context.SaveChanges();


        return Ok(ClientList);
    }

    [HttpPost("")]
    [Authorize(Roles=Role.ADMIN)]
    public ActionResult Clients(ClientData client)
    {
        bool response = false;
        
        context.Add<ClientData>(client);

        try
        {
            context.SaveChanges();
            response = true;
        }
        catch(DbUpdateException e)
        {
            Console.WriteLine($"Exception adding new client: {e.Message}");
        }

        if (response)
        {
            return Ok();
        }
        else
            return BadRequest();
    }

}