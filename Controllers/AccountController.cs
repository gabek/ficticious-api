﻿using System.IO;
using System.Runtime.Serialization.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

[Route("api/[controller]")]
[ApiController]
public class AccountController : ControllerBase
{
    private AuthService authService;

    public AccountController(AuthService authService)
    {
        this.authService = authService;
    }

    [HttpPost("[action]")]
    public ActionResult<User> Login(LoginRequest request)
    {
        User user = authService.Authenticate(request);
        if (user != null)
            return Ok(user);
        else
            return BadRequest(user);
    }

    [HttpPut("[action]")]
    [Authorize]
    public ActionResult<string> Change(ChangePassRequest request)
    {
        if (authService.ChangePassword(request))
            return Ok();
        else
            return BadRequest();
    }

    [HttpPost("[action]")]
    public ActionResult<User> Create(LoginRequest request)
    {
        User user = authService.CreateUser(request);
        if (user != null)
            return Ok(user);
        else
            return BadRequest(user);
    }

}
