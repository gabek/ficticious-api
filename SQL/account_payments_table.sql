DROP TABLE IF EXISTS account_payments;

CREATE TABLE account_payments
(
	username varchar(100) UNIQUE NOT NULL PRIMARY KEY,
    payment decimal(10,2) NOT NULL,
    purchase_date date,
    expiration_date date,
    FOREIGN KEY (username) REFERENCES client_data(username)
		ON DELETE CASCADE
)