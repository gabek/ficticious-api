DROP TABLE IF EXISTS users_table;

CREATE TABLE users_table
(
	username varchar(50) UNIQUE NOT NULL PRIMARY KEY,
    password varchar(255) NOT NULL,
    role ENUM("USER", "ADMIN") NOT NULL 
)