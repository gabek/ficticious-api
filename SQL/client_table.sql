DROP TABLE IF EXISTS account_payments;
DROP TABLE IF EXISTS client_data CASCADE;

CREATE TABLE client_data
(
	username varchar(100) UNIQUE NOT NULL PRIMARY KEY,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
    account_type ENUM('Free','Regular','Premium') NOT NULL,
	occupation varchar(50),
	location varchar(50)
);