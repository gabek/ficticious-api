DELETE FROM client_data;

INSERT INTO client_data VALUES("test_user1", "Adam", "Amys", "Regular", "Office Worker", "New York");
INSERT INTO client_data VALUES("test_user2", "Beatrice", "Bobby", "Free", "Plane Pilot", "Mexico");
INSERT INTO client_data (username,first_name,last_name,account_type,location) VALUES("test_user3", "Corey", "Charles", "Regular","Canada");
INSERT INTO client_data (username, first_name, last_name, account_type,occupation) VALUES("test_user4", "Deb", "Downer", "Premium", "Therapist");
INSERT INTO client_data (username, first_name, last_name, account_type) VALUES("test_user5", "Ed", "EddnEddie", "Free");

#INSERT INTO account_payments VALUES("test_user1", 3.99, '2018-02-20', '2019-11-20');
#INSERT INTO account_payments VALUES("test_user2", 0, '2018-08-05', '2038-01-19');
#INSERT INTO account_payments VALUES("test_user3", 3.99, '2017-12-05', '2019-10-19');
#INSERT INTO account_payments VALUES("test_user4", 8.99, '2019-01-01', '2020-06-13');