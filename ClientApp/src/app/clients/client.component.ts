import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { ClientData } from '../common/clientdata';
import { LoginService } from '../services/loginservice';
import { ClientService } from '../services/clientservice';


const red = "#ff0000";
const blue = "#0000ff";

@Component({
  selector: 'app-home',
  templateUrl: './client.component.html',
})
export class ClientComponent {
  public clients: ClientData[];
  public role: string;
  private token: string;

  public newMessage: string;

  

  constructor(private http: HttpClient, private clientService: ClientService, private loginService: LoginService) {
    this.clientService.initializeMessage();
    this.clientService.clients.subscribe(value => this.clients = value);
    this.clientService.responObvs.subscribe(value => this.newMessage = value);
    this.loginService.obvToken.subscribe(value => this.token = value);
    this.loginService.obvRole.subscribe(value => this.role = value);
  }


  public submitNewClient() {
    var username = (<HTMLInputElement>document.getElementById("clientname")).value;
    var firstname = (<HTMLInputElement>document.getElementById("firstname")).value;
    var lastname = (<HTMLInputElement>document.getElementById("lastname")).value;
    var account = (<HTMLInputElement>document.getElementById("account")).value;
    var occupation = (<HTMLInputElement>document.getElementById("occupation")).value;
    var location = (<HTMLInputElement>document.getElementById("location")).value;

    if (username == "" || firstname == "" || lastname == "") {

      this.newMessage = "Please fill out the username, firstname, and lastname fields.";
      (<HTMLInputElement>document.getElementById("newMessage")).style.color = red;
      return;
    }

    let client = new ClientData(username, firstname, lastname, account, occupation, location);

    this.clientService.postClient(client, this.token);

    (<HTMLInputElement>document.getElementById("newMessage")).style.color = blue;

    this.clearForm();
    
  }

  private clearForm() {
    (<HTMLInputElement>document.getElementById("clientname")).value ="";
    (<HTMLInputElement>document.getElementById("firstname")).value = "";
    (<HTMLInputElement>document.getElementById("lastname")).value = "";
    (<HTMLInputElement>document.getElementById("account")).value = "Free";
    (<HTMLInputElement>document.getElementById("occupation")).value = "";
    (<HTMLInputElement>document.getElementById("location")).value = "";
  }



}
