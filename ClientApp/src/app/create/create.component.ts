import { Component, Inject } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { LoginService } from "../services/loginservice";
import { LoginData } from "../common/logindata";
import { UserData } from "../common/userdata";

@Component({
  selector: 'app-home',
  templateUrl: './create.component.html'
})

export class CreateComponent {
  private color: string;
  public createMessage: string;

  constructor(private loginService: LoginService, private router: Router, private location: Location, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    if (loginService.getUser()) {
      this.location.replaceState('');
      this.router.navigate(['']);
    }

    this.loginService.obvName.subscribe(value => {
      if (value != "") {
        this.location.replaceState('');
        this.router.navigate(['']);
      }
    });
  }

  public createUser() {
    var username = (<HTMLInputElement>document.getElementById("newname")).value;
    var password = (<HTMLInputElement>document.getElementById("newword")).value;
    var repeat = (<HTMLInputElement>document.getElementById("repeat")).value;


    if (username == "" || password == "") {
      this.color = "#ff0000";
      (<HTMLInputElement>document.getElementById("createMessage")).style.color = this.color;
      this.createMessage = "Please fill out all the fields.";
      return;
    }

    if (password != repeat)
      return;
    let login = new LoginData(username, password);
    this.http.post<UserData>(this.baseUrl + "api/Account/Create", login).subscribe(
      result => {
        console.log(result);
        this.loginService.setUser(result);
      },
      (err: HttpErrorResponse) => {
        console.log(err.error);
        console.log(err.name);
        console.log(err.message);
        console.log(err.status)
        console.log(err);
        (<HTMLInputElement>document.getElementById("createMessage")).style.color = this.color;
        if (err.status == 400)
          this.createMessage = "Failed to create: " + username + ". Try another username.";
        else
          this.createMessage = "Error: " + err.statusText;
      }
    );
    this.clearForm();
  }

  private clearForm() {
    (<HTMLInputElement>document.getElementById("newname")).value = "";
    (<HTMLInputElement>document.getElementById("newword")).value = "";
    (<HTMLInputElement>document.getElementById("repeat")).value = "";
  }
}
