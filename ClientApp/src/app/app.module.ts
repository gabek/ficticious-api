import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ClientComponent } from './clients/client.component';
import { AccountComponent } from './account/account.component';
import { CreateComponent } from './create/create.component';


import { GenericPipe } from './common/genericpipe';
import { ClientService } from './services/clientservice';
import { LoginService } from './services/loginservice';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ClientComponent,
    AccountComponent,
    CreateComponent,
    GenericPipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'clients', component: ClientComponent },
      { path: 'account', component: AccountComponent },
      { path: 'create', component: CreateComponent }
    ])
  ],
  providers: [LoginService, ClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
