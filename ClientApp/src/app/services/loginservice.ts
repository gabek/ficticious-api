import { Injectable } from '@angular/core';
import { UserData } from '../common/userdata';
import { BehaviorSubject } from 'rxjs';

/**
 * Use of Angularfire base inspired by:
 * https://angularfirebase.com/lessons/sharing-data-between-angular-components-four-methods/
 **/

@Injectable()
export class LoginService {
  private user: UserData;
  private username = new BehaviorSubject("");
  private token = new BehaviorSubject("");
  private role = new BehaviorSubject("");

  public obvName = this.username.asObservable();
  public obvToken = this.token.asObservable();
  public obvRole = this.role.asObservable();

  constructor() {
    this.user = null;
  }

  public getUser() {
    return this.user;
  }

  public setUser(user: UserData) {
    this.user = user;
    if (user != null) {
      this.username.next(user.username);
      this.token.next(user.token);
      this.role.next(user.role);
    }
    else {
      this.username.next("");
      this.token.next("");
      this.role.next("");
    }
  }

  
}
