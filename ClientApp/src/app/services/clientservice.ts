import { Inject, Injectable } from '@angular/core';
import { Observable, timer, of, BehaviorSubject } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ClientData } from '../common/clientdata';



/**
 * Todo change to a set?
 * Based on:
 * https://blog.thoughtram.io/angular/2018/03/05/advanced-caching-with-rxjs.html
 **/

const CHECK = 60000;

@Injectable()
export class ClientService {
  private clientCache: Observable<ClientData[]>;
  private endpoint: string;
  private response = new BehaviorSubject("");

  public responObvs = this.response.asObservable();

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.endpoint = baseUrl + 'api/Client';
  }

  get clients() {
    if (!this.clientCache) {
      const time = timer(0, CHECK);
      this.clientCache = time.pipe(switchMap(_ => this.fetchClients()), shareReplay());
    }
    return this.clientCache;
  }

 
  public postClient(client: ClientData, token: string) {
    let options = {};

    let headers = new HttpHeaders({
      "Authorization": "Bearer " + token
    });
    options = { headers: headers };

    this.http.post(this.endpoint, client, options).subscribe(result => {
      console.log(result);

      this.response.next("Added " + client.username + " to database");
    },
      (error: HttpErrorResponse) => {
        console.log(error.name);
        console.log(error.message);
        console.log(error.error);
        if (error.status == 400)
          this.response.next("Failed to add " + client.username + ". Try another username.");
        else
          this.response.next("HTTP Error: " + error.statusText);
      }
    );
  }

  public initializeMessage() {
    this.response.next("");
  }

  private fetchClients() {
    return this.http.get<ClientData[]>(this.endpoint);
  }

  private addToClients(client: ClientData) {
    let array = new Array<ClientData>();
    this.clientCache.forEach(items => items.forEach(item => array.push(item)));
    array.push(client);
    this.clientCache = of(array);
  }
}
