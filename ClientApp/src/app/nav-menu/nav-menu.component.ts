import { Component, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LoginData } from '../common/logindata';
import { LoginService } from '../services/loginservice';
import { UserData } from '../common/userdata';

/**
 * Solution for getting values from html documents in type script here:
 * https://stackoverflow.com/questions/31922386/how-to-construct-typescript-object-with-html-input-values
 * 
 **/

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  public user: string;
  public loginMessage: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private service: LoginService) {
    this.service.obvName.subscribe(value => this.user = value);
    this.loginMessage = "";
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  public login() {
    var username = (<HTMLInputElement>document.getElementById("username")).value;
    var password = (<HTMLInputElement>document.getElementById("password")).value

    if (username == "" || password == "") {
      this.loginMessage = "Please fill in both fields."
      return;
    }
    let data = new LoginData(username, password);
    this.http.post<UserData>(this.baseUrl + "api/Account/Login", data).subscribe(
      result => {
        console.log(result);
        if (result)
          this.loginMessage = "";
        this.service.setUser(result);
        
      },
      (error: HttpErrorResponse) => {
        if (error.status == 400)
          this.loginMessage = "Bad username or password.";
        else
          this.loginMessage = "Error: " + error.statusText;
        console.log(error.error);
        console.log(error.name);
        console.log(error.message);
        console.log(error.status)
        console.log(error);
      }
    );

    (<HTMLInputElement>document.getElementById("username")).value = "";
    (<HTMLInputElement>document.getElementById("password")).value = "";
  }

  public logout() {
    this.service.setUser(null);
    this.user = null;
  }
}
