"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ChangePassData = /** @class */ (function () {
    function ChangePassData(username, currentPassword, newPassword) {
        this.username = username;
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }
    return ChangePassData;
}());
exports.ChangePassData = ChangePassData;
//# sourceMappingURL=changepassdata.js.map