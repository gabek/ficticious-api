export class ChangePassData {

  constructor(private username: string, private currentPassword: string, private newPassword: string) {
  }
}
