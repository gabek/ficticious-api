import { Account } from './account';

export class ClientData {
  username: string;
  firstname: string;
  lastname: string;
  account: string;
  occupation: string;
  location: string;

  constructor(username: string, firstname: string, lastname: string, account: string, occupation: string, location: string) {
    this.username = username;
    this.firstname = firstname;
    this.lastname = lastname;
    this.account = account;
    this.occupation = occupation;
    this.location = location;
  }
}
