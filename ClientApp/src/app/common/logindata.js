"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoginData = /** @class */ (function () {
    function LoginData(username, password) {
        this.username = username;
        this.password = password;
    }
    return LoginData;
}());
exports.LoginData = LoginData;
//# sourceMappingURL=logindata.js.map