import { Injectable, Pipe, PipeTransform } from '@angular/core';


/**
 * Pipe based on https://seegatesite.com/angular-4-tutorial-create-custom-search-filter-pipe-in-html-table/
 **/

@Pipe({
  name: 'genericpipe'
})

@Injectable()
export class GenericPipe implements PipeTransform {
  transform(items: any[], search: string, param: string): any {
    if (!items) return [];
    else if (!search) return items;
    else if (!param || param == '') return [];
    else return items.filter(item => item[param].toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) > -1);

  }
}
