export interface UserData {
  username: string;
  role: string;
  token: string;
}
