import { Component, Inject } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { LoginService } from "../services/loginservice";
import { ChangePassData } from "../common/changepassdata";

@Component({
  selector: 'app-home',
  templateUrl: './account.component.html'
})

/**
 * Redirection based on:
 * https://stackoverflow.com/questions/34331478/angular-redirect-to-login-page
 **/

export class AccountComponent {
  private user: string;
  private token: string;
  private color: string;
  public passwordMessage: string;

  constructor(private loginService: LoginService, private router: Router, private location: Location, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    if (!loginService.getUser()) {
      this.location.replaceState('');
      this.router.navigate(['']);
    }
    this.passwordMessage = "";
    this.color = "";
    this.loginService.obvToken.subscribe(value => this.token = value);
    this.loginService.obvName.subscribe((value) => {
      this.user = value;
      if (value == "") {
        this.location.replaceState('');
        this.router.navigate(['']);
      }
    }
    );
  }

  public changePassword() {
    var current = (<HTMLInputElement>document.getElementById("current")).value;
    var password = (<HTMLInputElement>document.getElementById("new")).value;
    var repeat = (<HTMLInputElement>document.getElementById("repeatNew")).value;

    if (current == "" || password == "" || repeat == "") {
      this.color = "#ff0000";
      (<HTMLInputElement>document.getElementById("passwordMessage")).style.color = this.color;
      this.passwordMessage = "Please fill out all the fields.";
      return;
    }

    if (password != repeat) {
      this.color = "#ff0000";
      (<HTMLInputElement>document.getElementById("passwordMessage")).style.color = this.color;
      this.passwordMessage = "New passwords do not match.";
      return;
    }

    let change = new ChangePassData(this.user, current, password);
    let options = {};

    let headers = new HttpHeaders({
      "Authorization": "Bearer " + this.token
    });
    options = { headers: headers };

    this.http.put(this.baseUrl + "api/Account/Change", change, options).subscribe(result => {
      console.log(result);
      
        this.color = "#0000ff";
        (<HTMLInputElement>document.getElementById("passwordMessage")).style.color = this.color;
        this.passwordMessage = "Successfully changed passwords.";
      
      //else {
     //   this.color = "#ff0000";
    //   (<HTMLInputElement>document.getElementById("passwordMessage")).style.color = this.color;
     //   this.passwordMessage = "Current password was incorrect.";
      //}
    },
      (error: HttpErrorResponse) => {
        this.color = "#ff0000";
        (<HTMLInputElement>document.getElementById("passwordMessage")).style.color = this.color;
        if (error.status == 400)
          this.passwordMessage = "Current password was incorrect.";
        else
          this.passwordMessage = "Error: " + error.statusText;
        console.log(error.error);
        console.log(error.message);
      }
    );
    this.clearForm();
  }

  private clearForm() {
    (<HTMLInputElement>document.getElementById("current")).value = "";
    (<HTMLInputElement>document.getElementById("new")).value = "";
    (<HTMLInputElement>document.getElementById("repeatNew")).value = "";
  }

}
