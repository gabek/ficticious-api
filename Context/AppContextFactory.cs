﻿using System;
using System.IO;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

/**
 * Based on: https://codingblast.com/entityframework-core-idesigntimedbcontextfactory/
 */

public class AppContextFactory : IDesignTimeDbContextFactory<AppContext>
{
    public AppContext CreateDbContext(string[] args)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();

        DbContextOptionsBuilder builder = new DbContextOptionsBuilder();

        string connection = configuration.GetConnectionString("AppConnString");

        builder.UseMySQL(connection);


        return new AppContext(builder.Options);
    }

}


