﻿using System.Collections.Generic;
using MySql.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using Microsoft.Extensions.Configuration;

public class AppContext : DbContext
{
    public DbSet<UserDB> Users { get; set; }
    public DbSet<ClientData> Clients { get; set; }

    public AppContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<UserDB>(entity =>
        {
            entity.HasKey(e => e.Username);
            entity.Property(e => e.Username).IsRequired().HasColumnName("username");
            entity.Property(e => e.Role).IsRequired().HasColumnName("role");
            entity.Property(e => e.Password).IsRequired().HasColumnName("password");
            entity.ToTable("users_table");
        });

        modelBuilder.Entity<ClientData>(entity =>
        {
            entity.HasKey(e => e.Username);
            entity.Property(e => e.Username).IsRequired().HasColumnName("username");
            entity.Property(e => e.Firstname).IsRequired().HasColumnName("first_name");
            entity.Property(e => e.Lastname).IsRequired().HasColumnName("last_name");
            entity.Property(e => e.Account).IsRequired().HasColumnName("account_type");
            entity.Property(e => e.Occupation).HasColumnName("occupation");
            entity.Property(e => e.Location).HasColumnName("location");
            entity.ToTable("client_data");
        });
    }

}
