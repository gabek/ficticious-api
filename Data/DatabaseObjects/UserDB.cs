﻿public class UserDB
{
    public string Username { get; set; }

    public string Password { get; set; }

    public string Role { get; set; }

    public UserDB(string username, string password, string role)
    {
        Username = username;
        Password = password;
        Role = role;
    }
}

