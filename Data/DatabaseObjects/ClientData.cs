﻿
public class ClientData
{
    public string Username { get; }

    public string Firstname { get; }

    public string Lastname { get; }

    public string Account { get; }

    public string Occupation { get; }

    public string Location { get; }



    public ClientData(string username, string firstname, string lastname, string account, string occupation, string location)
    {
        Username = username;
        Firstname = firstname;
        Lastname = lastname;
        Account = account;
        Occupation = occupation;
        Location = location;
    }
}
