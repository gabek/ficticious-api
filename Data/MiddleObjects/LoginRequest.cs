﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class LoginRequest
{
    public string Username { get; }

    public string Password { get; }

    public LoginRequest(string username, string password)
    {
        Username = username;
        Password = password;
    }
}

