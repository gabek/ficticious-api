﻿public class ChangePassRequest
{
    public string Username { get; }
    public string CurrentPassword { get; }
    public string NewPassword { get; }

    public ChangePassRequest(string username, string currentPassword, string newPassword)
    {
        Username = username;
        CurrentPassword = currentPassword;
        NewPassword = newPassword;
    }
}