﻿using System;
using System.Runtime.Serialization;

[DataContract]
public class User : IExtensibleDataObject
{
    [DataMember(Name = "username")]
    public string Username { get; set; }

    [DataMember(Name = "role")]
    public string Role { get; set; }

    [DataMember(Name = "token")]
    public string Token { get; set; }

    public User()
    {

    }

    public User(String username, String role)
    {
        this.Username = username;
        this.Role = role;
    }

    public ExtensionDataObject ExtensionData { get; set; }

}

